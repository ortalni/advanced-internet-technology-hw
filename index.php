<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
//use Chatter\Middleware\Logging;
$app = new \Slim\App();
//$app->add(new Logging());

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
//1
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('The customer number is '.$args['number']);
});
//2
$app->get('/customers/{number}/products/{pnumber}', function($request, $response,$args){
   return $response->write('The customer number is '.$args['number'].' ' . 'and the product number is '.$args['pnumber'] );
});

$app = new \Slim\App();
//ex1
$app->get('/customers/{id}', function($request, $response, $array){
	$json = '{"1":"john", "2":"jack"}';
	$array = (json_decode($json));
	$id = $request->getAttribute('id');
	if (isset($array->{$id})) {
        $name = $array->{$id};
        return $response->write('Hello, '.$name);
	} else {
		$result = "User with id ". $id. " not exist.";	
	}
	
   $response->getBody()->write($result);
    return $response;
});

$app->get('/messages', function($request, $response,$args){
   $_message = new Message(); 
   $messages = $_message->all(); 
   
   $payload = [];
   foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];

   }
    return $response->withStatus(200)->withJson($payload);
    
   // return $response->write('Working');

});

$app->get('/messages/{id}', function($request, $response,$args){
    $_id = $args['id']; 
    $message = Message::find($_id); 
    return $response->withStatus(200)->withJson($message);
 });

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message', '');
    $user_id = $request->getParsedBodyParam('user_id', '');
    $_message = new Message();
    $_message->body = $message; 
    $_message->user_id = $user_id;
    $_message->save();
    
    
    if($_message->id){
        $payload = ['message_id'=>$_message->id];
        return $response->withStatus(200)->withJson($payload);
    } else {
        return $response->withStatus(400); 
    } 
 
 });

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $_message =Message::find($args['message_id']);
    $_message->delete();
    if($message->exists){
        return $response->withStatus(400);
    }
    else{
        return $response->withStatus(200);

    }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
   $message = $request->getParsedBodyParam('message','');// מגיע מהפוסט
   $user_id = $request->getParsedBodyParam('user_id', '');   
   $_message = Message::find($args['message_id']); // אובייקט מהדטה בייס
    //die("Message id". $_message->id);
   $_message->body = $message; //עדכון body 
   // תרגיל 6
   $_message->user_id = $user_id;
   if($_message->save()){
        $payload = ['message_id'=>$_message->id,"result"=>"The message has been updated seccessfully"];
        return $response->withStatus(200)->withJson($payload);
   }
   else { 
       return $response->withStatus(400);
   }
  
});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the json to array
    Message::insert($payload); //incluse save
    return $response->withStatus(201)->withJson($payload);
      
 });

// Ex2 user crud

$app->get('/users', function($request, $response,$args){ //read
    $_user = new User();
    $users = $_user->all(); //מערך של אוביקטים 
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'username'=>$usr->username,
            'password'=>$usr->password,
            'email'=>$usr->email
        ];        
    }
    return $response->withStatus(200)->withJson($payload);
 });
 
 $app->post('/users', function($request, $response,$args){ //create
    $username = $request->getParsedBodyParam('username', '');
    $password = $request->getParsedBodyParam('password', '');
    $email = $request->getParsedBodyParam('email', '');
    $_user = new User();
    $_user->username = $username; 
    $_user->password = $password;
    $_user->email = $email;
    try {
     $_user->save();
    } catch (Illuminate\Database\QueryException $e) {
        var_dump($e->errorInfo);
    }
    
    if($_user->id){
        $payload = ['user_id'=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400); 
    } 
 
 });
 
 $app->delete('/users/{user_id}', function($request, $response,$args){ 
   $user = User::find($args['user_id']);
   $user->delete(); 
   if($user->exists){
        return $response->withStatus(400);
    } else {
        return $response->withStatus(200);
    }
 
 });

 $app->put('/users/{id}', function($request, $response,$args){
    $_username = $request->getParsedBodyParam('username','');// מגיע מהפוסט
    $_user = User::find($args['id']); // אובייקט מהדטה בייס
     //die("Message id". $_message->id);
    $_user->username = $_username; //עדכון  
    if($_user->save()){
         $payload = ['id'=>$_user->id,"result"=>"The user has been updated seccessfully"];
         return $response->withStatus(200)->withJson($payload);
    }
    else { 
        return $response->withStatus(400);
    }
   
 });
//Ex3 user bulk
 $app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the json to array
    User::insert($payload); //incluse save
    return $response->withStatus(201)->withJson($payload);
      
 });
 /*
 $app->delete('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody(); //convert the json to array
    User::insert($payload); //incluse save
    return $response->withStatus(201)->withJson($payload);
      
 });
 */
 $app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();